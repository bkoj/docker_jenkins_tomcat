FROM tomcat:9.0


RUN cp -r webapps.dist/* webapps/
COPY tomcat-users.xml conf/

EXPOSE 8081

CMD ["catalina.sh","run"]
