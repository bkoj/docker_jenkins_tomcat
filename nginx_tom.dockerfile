FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
COPY http.conf /etc/nginx/conf.d/

RUN mkdir /etc/nginx/certificats/

COPY nginx_* /etc/nginx/certificats/

EXPOSE 443
